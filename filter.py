import pandas as pd
import re

"""Есть датасет содержащий 3 колонки смешанных типов

A,B,F
a1,,1 
a2,  ,
3,e,1
a4,3,2 -- неправильный фильтр

1) отфильтровать строки содержащие пустые ячейки колонок А и B

2) вывести число отфильтрованных строк

пустая ячейка, это ячейка содержащая 0 символов или некое множество непечатаемых символов

3) после операций 1) и 2) отфильтровать результат по колонке F: оставить только те строки которые содержат F==1. 
  Колонка F может содержать также неправильные значения, например 2. Такие строки нужно вывести на печать.
Есть датасет содержащий 3 колонки смешанных типов

A,B,F
a1,,1 
a2,  ,
3,e,1
a4,3,2 -- неправильный фильтр

1) отфильтровать строки содержащие пустые ячейки колонок А и B

2) вывести число отфильтрованных строк

пустая ячейка, это ячейка содержащая 0 символов или некое множество непечатаемых символов

3) после операций 1) и 2) отфильтровать результат по колонке F: оставить только те строки которые содержат F==1. 
  Колонка F может содержать также неправильные значения, например 2. Такие строки нужно вывести на печать.
"""

# initialize data frame
data_frame = pd.DataFrame(index=range(4), columns=["A","B","F"])

data_frame.A = ['a1','a2',3,'a4']
data_frame.B = ['', ' ', 'e', 3]
data_frame.F = [1, '', 1, 2]

print('Initial data set\n', data_frame)

filter = data_frame[['A','B']].apply(lambda x: x.str.contains('^\s*$', regex=True)).any(axis=1)
filtered_set = data_frame[~filter]

print('\n1) Filtered row count =', len(filtered_set))
print(filtered_set,"\n")

print('\n2) Invalid rows:')

for index, row in filtered_set.iterrows():
    if row['F'] != 1:
        print(row)
        # since drop returns new object, we have to reassign value of  filtered data set
        filtered_set = filtered_set.drop(index)

print('\nRemaining rows:')
print(filtered_set)





